<?php
$username = $_POST['username'];
$passwd = $_POST['passwd'];

if( (!isset($username) && !isset($passwd)) || ($username == '' && $passwd == '')){
    $raw = array('code' => 1 , 'data' => array('msg' => '请提供用户名与密码'));
    echo json_encode($raw , JSON_UNESCAPED_UNICODE);
    exit(1);
}

if(!isset($username) || $username == ''){
    $raw = array('code' => 1 , 'data' => array('msg' => '请提供用户名'));
    echo json_encode($raw , JSON_UNESCAPED_UNICODE);
    exit(1);
}

if(!isset($passwd) || $passwd == ''){
    $raw = array('code' => 1 , 'data' => array('msg' => '请提供密码'));
    echo json_encode($raw , JSON_UNESCAPED_UNICODE);
    exit(1);
}

include '../config/database.php';

$con = mysqli_connect($dbhost,$dbuser,$dbpasswd,$dbname);

$sqlname = $dbprefix.'users';

$sqlcommand = "SELECT * FROM `".$sqlname."` WHERE `username` LIKE '".$username."'";

$result = mysqli_query($con, $sqlcommand);
if (mysqli_num_rows($result) > 0) {
    while($row = mysqli_fetch_assoc($result)) {
        $truepwss = $row['passwd'];
        $trueusname = $row['username'];
    }
} else {
    $raw = array("code" => 1 , 'data' => array('msg' => '用户名或密码错误'));
    echo json_encode($raw , JSON_UNESCAPED_UNICODE);
    exit(1);
}

if(sha1($passwd) == $truepwss){

    session_start();

    $_SESSION['username'] = $trueusname;
    $_SESSION['passwd'] = $passwd;
    $_SESSION['login'] = true;

    $raw = array("code" => 0 , 'data' => array('msg' => '登陆成功'));
    echo json_encode($raw , JSON_UNESCAPED_UNICODE);
    exit(1);
    
}else {
    $raw = array("code" => 1 , 'data' => array('msg' => '用户名或密码错误'));
    echo json_encode($raw , JSON_UNESCAPED_UNICODE);
    exit(1);
}


?>