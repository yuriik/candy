<?php

include "../config/database.php";

$con = mysqli_connect($dbhost,$dbuser,$dbpasswd,$dbname);

$sqlname = $dbprefix.'messages';

$sql = "SELECT * FROM `".$sqlname.'`';

$result = mysqli_query($con, $sql);

$list = array();

$i=0;

if (mysqli_num_rows($result) > 0) {
    while($row = mysqli_fetch_assoc($result)) {
        $list[$i] = intval($row['id']);
        $i++;
    }
    $raw = array('code' => 0 , 'data'=>array('array' => $list));
    echo json_encode($raw , JSON_UNESCAPED_UNICODE);
} else {
    $raw = array('code' => 1 , 'data'=>array('msg' => "暂时没有留言"));
    echo json_encode($raw , JSON_UNESCAPED_UNICODE);
}

?>