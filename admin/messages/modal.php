        <div class="modal fade" id="deldone" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true" data-bs-backdrop="static">
			<div class="modal-dialog modal-dialog-centered">
				<div class="modal-content">
					<div class="modal-header">
						<h5 class="modal-title" id="deldoneLabel">删除成功</h5>
						<button type="button" class="btn" data-bs-dismiss="modal" aria-label="Close" onclick = "location.reload();"><i class="fa fa-times"></i></button>
					</div>
					<div class="modal-body">
						<span id="delmhtmsg">Loading...</span>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-primary" onclick = "location.reload();">确认</button>
					</div>
				</div>
			</div>
		</div>

        <div class="modal fade" id="delcheck" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
			<div class="modal-dialog modal-dialog-centered">
				<div class="modal-content">
					<div class="modal-header">
						<h5 class="modal-title" id="deldoneLabel">确认要删除该语句吗？</h5>
						<button type="button" class="btn" data-bs-dismiss="modal" aria-label="Close"><i class="fa fa-times"></i></button>
					</div>
					<div class="modal-body">
						语句删除后将会无法恢复，请在此确认你的操作
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-secondary" data-bs-dismiss="modal">取消</button>
                        <div id="ocevent">
						    
                        </div>
					</div>
				</div>
			</div>
		</div>


        <div class="modal fade" id="lazy" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
			<div class="modal-dialog modal-dialog-centered">
				<div class="modal-content">
					<div class="modal-header">
						<h5 class="modal-title" id="deldoneLabel">懒得写了</h5>
						<button type="button" class="btn" data-bs-dismiss="modal" aria-label="Close"><i class="fa fa-times"></i></button>
					</div>
					<div class="modal-body">
						懒得写了
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-secondary" data-bs-dismiss="modal">关闭</button>
					</div>
				</div>
			</div>
		</div>