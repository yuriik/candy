<!DOCTYPE html>

<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>语句列表 - 棉花糖</title>

		<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
		<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/admin-lte@3.1.0/dist/css/adminlte.min.css" integrity="sha256-FQ+k0mIFfWXVTaW1arh3qKx8IXX5Bm5f6QG+0pkUjaE=" crossorigin>
		<link rel="preload" as="font" href="https://cdn.jsdelivr.net/npm/@fortawesome/fontawesome-free@5.13.0/webfonts/fa-solid-900.woff2" crossorigin>
		<link rel="preload" as="font" href="https://cdn.jsdelivr.net/npm/@fortawesome/fontawesome-free@5.13.0/webfonts/fa-regular-400.woff2" crossorigin>
		<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/@fortawesome/fontawesome-free@5.13.0/css/all.min.css" integrity="sha256-h20CPZ0QyXlBuAw7A+KluUYx/3pK+c7lYEpqLTlxjYQ=" crossorigin>
		<link rel="stylesheet" href="https://cdn.bootcss.com/ionicons/2.0.1/css/ionicons.min.css">
	</head>
	<body class="hold-transition sidebar-mini">

		<?php include './modal.php' ?>

		<div class="wrapper">

			<nav class="main-header navbar navbar-expand navbar-white navbar-light">
				<ul class="navbar-nav">
					<li class="nav-item">
						<a class="nav-link" data-widget="pushmenu" href="#" role="button">
							<i class="fas fa-bars"></i>
						</a>
					</li>
				</ul>

				<ul class="navbar-nav ml-auto">
					<li class="nav-item">
						<a class="nav-link" href="" role="button" onclick="logout()">
							<i class="fas fa-sign-out-alt"></i>
						</a>
					</li>
				</ul>
			</nav>


			<aside class="main-sidebar sidebar-dark-primary elevation-4">

				<a class="brand-link" style="text-align: center;">
					<span class="brand-text font-weight-light">棉花糖后台管理</span>
				</a>


				<div class="sidebar">

					<div class="user-panel mt-3 pb-3 mb-3 d-flex">
						<div class="image">
							<img src="https://gravatar.loli.net/avatar/" class="img-circle elevation-2" alt="User Image">
						</div>
						<div class="info">
							<a class='d-block'>
								<span id="usname">Loading...</span>
							</a>
						</div>
					</div>

					<nav class="mt-2">
						<ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
							<li class="nav-header">设置</li>
							<li class="nav-item">
								<a href="../" class="nav-link">
									<i class="nav-icon fas fa-home"></i>
									<p>仪表盘</p>
								</a>
							</li>

							<li class="nav-item">
								<a href="./" class="nav-link active">
									<i class="nav-icon fas fa-list"></i>
									<p>语句列表</p>
								</a>
							</li>

							<li class="nav-item">
								<a href="../account" class="nav-link">
									<i class="nav-icon fas fa-user"></i>
									<p>账户管理</p>
								</a>
							</li>

							<li class="nav-item">
								<a href="../site/" class="nav-link">
									<i class="nav-icon fas fa-stream"></i>
									<p>站点设置</p>
								</a>
							</li>
							<div>
								<li class="nav-header">快捷链接</li>
								<li class="nav-item">
									<a href="../../" class="nav-link">
										<i class="nav-icon fas fa-home"></i>
										<p>主页</p>
									</a>
								</li>
								<li class="nav-item">
									<a href="../../read" class="nav-link">
										<i class="nav-icon fas fa-book"></i>
										<p>阅读棉花糖</p>
									</a>
								</li>
							</div>
						</ul>
					</nav>

				</div>

			</aside>


			<div class="content-wrapper">

				<div class="content-header">
					<div class="container-fluid">
						<div class="row mb-2">
							<div class="col-sm-6">
								<h1 class="m-0">语句列表</h1>
							</div>
							<div class="col-sm-6">
								<ol class="breadcrumb float-sm-right">
									<li class="breadcrumb-item">
										<a href="#">管理面板</a>
									</li>
									<li class="breadcrumb-item active">语句列表</li>
								</ol>
							</div>
						</div>
					</div>
				</div>

				<div class="content">
					<div class="container-fluid">
						<div class="row">
							<table class="table">
								<thead>
									<tr>
										<th scope="col">#</th>
										<th scope="col">概览</th>
										<th scope="col">操作</th>

									</tr>
								</thead>
								<tbody>
									<?php include 'table.php'; ?>
								</tbody>
							</table>
						</div>
					</div>
				</div>

			</div>

			<footer class="main-footer">
				<strong>Copyright &copy; 2021 <a href="https://i.lite.cafe" target="_blank">ImJingLan</a>.</strong> All rights reserved.
			</footer>
		</div>
		<script src="https://cdn.jsdelivr.net/npm/jquery@3.6.0/dist/jquery.min.js"></script>
		<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/js/bootstrap.bundle.min.js" integrity="sha384-gtEjrD/SeCtmISkJkNUaaKMoLD0//ElJ19smozuHV6z3Iehds+3Ulb9Bn9Plx0x4" crossorigin="anonymous"></script>
		<script src="https://cdn.jsdelivr.net/npm/admin-lte@3.1.0/dist/js/adminlte.min.js"></script>
		<script src="../js/Y8ui.js"></script>
		<script>
			checklogin();
						            getuser();
		</script>
		<script>
			function logout()
						            {
						                $.ajax({
						                    type: "post",
						                    url: "/admin/logout",
						                    data: {},
						                    async: false,
						                    success: function(a) {
						                    }
						                });
						                window.location.href="/login/";
						            }
						
						function delcheck(cid){
							$('#ocevent').html('<button type="button" class="btn btn-primary" data-bs-dismiss="modal" data-bs-toggle="modal" data-bs-target="#deldone" onclick="del('+cid.toString()+')">确认</button>')
							$('#delcheck').modal('show');
						}
			
						function del(cid){
							$.ajax({
						    	type: "post",
						        url: "./plugins/del.php",
						        data: {cid:cid},
						        async: false,
						        success: function(a) {
									json = JSON.parse(a);
									if(json['code'] == 0){
										
										
										$('#delmhtmsg').text(json['data']['msg']);
										$('#deldone').modal('show');
									}
						        }
						    });
						}
		</script>
	</body>
</html>