<?php

$oldpw = $_POST['oldpw'];

$newpw = $_POST['newpw'];

$confirmpw = $_POST['confirmpw'];

if($newpw==$confirmpw && $newpw!='' && $confirmpw!='' && $oldpw != ''){

    session_start();

    include '../../config/database.php';
    $con = mysqli_connect($dbhost,$dbuser,$dbpasswd,$dbname);

    $sqlname = $dbprefix.'users';
    $sql = "SELECT passwd as sha1pw FROM `".$sqlname."` WHERE `username` LIKE '".$_SESSION['username']."'";
    $a     = mysqli_query( $con, $sql );
    $b     = mysqli_fetch_assoc( $a );
    $truesha1pw = $b['sha1pw'];
    if($truesha1pw == sha1($oldpw)){
        $sql = "UPDATE `".$sqlname."` SET `passwd` = '".sha1($newpw)."` WHERE `username` LIKE '".$_SESSION['username']."'";
        $a = mysqli_query( $con, $sql );
        echo json_encode(  array(  'code' => 0 ,'data' => array('msg' => "密码修改成功，请重新登陆" )  )  );
        exit(1);
    }

    if($truesha1pw != sha1($oldpw)){
        echo json_encode(  array(  'code' => 1 ,'data' => array('msg' => "旧密码不正确，请重新输入" )  )  );
        exit(1);
    }
}
else{
    if($newpw=='' || $confirmpw=='')
    {echo json_encode(  array(  'code' => 1 ,'data' => array('msg' => "新密码不能为空，请重新输入" )  )  ); exit(1);}
    if($newpw!=$confirmpw)
    {    echo json_encode(  array(  'code' => 1 ,'data' => array('msg' => "两次新密码不相等，请重新输入" )  )  ); exit(1);}
    if($oldpw == '')
    {echo json_encode(  array(  'code' => 1 ,'data' => array('msg' => "旧密码不能为空，请重新输入" )  )  ); exit(1);}
}
?>